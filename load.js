window.onload = function(){
    loadUI();
    game.initialize();
};
function loadUI() {
    new screen({ 
        id: 'container',
        items: [
            {
                source: {
                    type: 'ui_template',
                    uri: 'history/panel'
                }
            },
            {
                source: {
                    type: 'ui_template',
                    uri: 'action/panel'
                }
            },
            {
                source: {
                    type: 'ui_template',
                    uri: 'inventory/panel'
                }
            }
        ]
    }).loadInto('body');
};


