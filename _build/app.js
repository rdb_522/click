action_handler = {

    handle(e) {
        if (e.target){
            if (e.target.dataset){
                if (e.target.dataset.action){
                    let actionData = e.target.dataset.action;
                    if (action[actionData]){
                        actionData = action[actionData];
                        if (actionData.requires){
                            //if requires property is a function
                            if (typeof actionData.requires == 'function'){
                                if(!actionData.requires(e)){
                                    log.add('requirements not met for: ' + actionData.text);
                                    return false;
                                }
                            //if the requires property is the standard 'check/requires' object
                            } else if (Array.isArray(actionData.requires)) {
                                let errors = this.processRequirements(actionData.requires);
                                if (errors.length > 0) {
                                    errors.forEach((error)=>{
                                        error.call();
                                    })
                                    return false;
                                }
                            }
                        }
                        actionData.action(e);
                        return true;
                    } else {
                        console.log('cannot locate action: ' + actionData)
                        return false;
                    }
                }
            }
        }
        console.log(e);
    },
    processRequirements(needs){
        let errors = [];
        needs.forEach((item) => {
            if (!item.check.call()){
                errors.push(item.fail);
            }
        });
        return errors;
    }
}
clock = {
    state: {
        started: false,
        running: false,
    },
    initialize(){
        _event.subscribe('pause',this);
        this.run();
        this.state.started = true;
    },

    stop(){
        window.clearInterval(gameTime);
        this.state.running = false;
    },

    run(){
        this.state.running = true;
        window.gameTime = window.setInterval(function(){
            _event.fire('tick');
        }, 1000);
    }

}
game = {
    inventory: {
        personal: []
    },

    state: {
        stage: 1
    },

    getInventory(location){
        if (this.inventory[location]){
            return this.inventory[location].sort((a,b) => {
                return a.id > b.id;
            });
        } else {
            return [];
        }
    },

    addInventory(location, id, qty){
        let found = this.inventory[location].find((i) =>{
            return i.id == id
        });
        if (found){
            found.qty += qty;
        } else {
            this.inventory[location].push({
                id: id,
                qty: qty,
                max: qty
            });
        }
        _event.fire('inventory-update');
    },

    initialize(){
        _event.subscribe('tick', this);
        player.initialize();
        clock.initialize();
    },

    tick(){
        player.tickStats();
    }
}
log = {
    items: [],
    
    add(item){
        log.items.push(item);
        if (this.items.length > 20){
            let out = this.items.shift();
        }
        _event.fire('logUpdate');
    },

    getItems(){
        //array.reverse() permanents reverses the array, so this reversing is done manually
        let reversed = [];
        for (let i = this.items.length - 1; i >= 0; i--){
            reversed.push(this.items[i]);
        }
        return reversed;
    }
}
player = {
    stats: [],

    initialize(){
        this.addStat({health: 10, stamina: 10});
        this.changeStatMax({health: 10, stamina: 10});
    },

    getStats(){
        return this.stats.sort((a,b) => {
            return a.id > b.id;
        });
    },

    getStat(id){
        let found = this.stats.find((i) =>{
            return i.id == id
        });
        if (found){
            return found.qty
        } else {
            return false;
        }
    },

    changeStatMax(data){
        if (typeof data == 'array'){
            data.foreach((statList) => {
                this.changeStatMax(statList);
            })
        } else if(typeof data == 'object') {
            Object.keys(data).forEach((stat) => {
                let found = this.stats.find((i) =>{
                    return stat == i.id
                });
                if (found){
                    found.max = data[stat];
                } else {
                    this.stats.push({
                        id: stat,
                        qty: 0,
                        max: data[stat]
                    });
                }
                
            })
        }
        _event.fire('player-stats-update');
    },

    addStat(data) {
        if (typeof data == 'array'){
            data.foreach((statList) => {
                this.addStat(statList);
            })
        } else if(typeof data == 'object') {
            Object.keys(data).forEach((stat) => {
                let found = this.stats.find((i) =>{
                    return stat == i.id
                });
                if (found){
                    found.qty += data[stat];
                } else {
                    this.stats.push({
                        id: stat,
                        qty: data[stat]
                    });
                }
                
            })
        }
        _event.fire('player-stats-update');
    },

    tickStats(){
        this.stats.forEach((stat) => {
            stat.qty = stat.qty >= stat.max ? stat.qty : stat.qty + 1;
        })
        _event.fire('player-stats-update');
    }
}
action_tab = {};
action = {};
ui_template = {};

action_tab['gang'] =  [
    {
        id: 'rock',
        text: 'grab rock',
        action: 'player/mine/metal'
    },
]
action_tab['mine'] =  [
    {
        id: 'sand',
        text: 'grab sand',
        action: 'player/mine/sand',
        info: '1 stamina'
    },
    {
        id: 'rock',
        text: 'grab rock',
        action: 'player/mine/rock',
        info: '2 stamina'
    },
    {
        id: 'metal',
        text: 'mine metal',
        action: 'player/mine/metal',
        info: '5 stamina'
    },
]
action['system/info'] =  {
    action: function (e){
        _event.fire('inventory-update');
    }
}
action['system/pause'] =  {
    action: function (e){
        if (clock.state.running){
            clock.stop();
        } else {
            clock.run();
        }
        console.log(clock.state.running ? 'Clock Running' : 'Clock Stopped');
    }
}
ui_template['action/panel'] =  {
    id: 'action',
    type: 'panel',
    items: [
        {
            source: {
                type: 'ui_template',
                uri: 'action/tabs/tabs'
            }
        }
    ]
};
ui_template['history/panel'] =  {
    id: 'history',
    type: 'panel',
    items: [
        {
            source: {
                type: 'ui_template',
                uri: 'history/menu/buttons'
            }
        },
        {
            source: {
                type: 'ui_template',
                uri: 'history/log/list'
            }
        }
    ]
};
ui_template['inventory/panel'] =  {
    id: 'inventory',
    type: 'panel',
    items: [
        {
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'inventory/stats/table'
            }
        },
        {
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'inventory/personal/table'
            }
        }
    ]

};
action['player/mine/metal'] =  {
    text: 'mine metal',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 5;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -5});
        game.addInventory('personal', 'metal', 1);
    }
}
action['player/mine/rock'] =  {
    text: 'grab rock',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 2;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -2});
        game.addInventory('personal', 'rock', 1);
    }
}
action['player/mine/sand'] =  {
    text: 'grab sand',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 1;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -1});
        game.addInventory('personal', 'sand', 1);
    }
}
ui_template['action/tabs/tabs'] =  {
    id: 'action_tabs',
    type: 'tab_set',
    items: [
        {
            title: 'mine',
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'action/tabs/mine/table'
            }
        },
        {
            title: 'gang',
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'action/tabs/gang/table'
            }
        }
    ],
    state: {
        selected: 'mine'
    }
    
};
ui_template['history/log/list'] =  {
    id: 'log',
    type: 'list',
    items: {
        source: {
            type: 'var',
            uri: 'log/getItems'
        }
    },
    subscribe: {
        'logUpdate': function(){
            this.reload();
        }
    }
};
ui_template['history/menu/buttons'] =  {
    id: 'menu',
    type: 'button_set',
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
    items: [
        {
            id: 'pause',
            action: 'system/pause'
        },
        {
            id: 'about',
            action: 'system/info',
            text: 'info'
        }
    ]
}
ui_template['inventory/personal/table'] =  {
    id: 'inventory_personal',
    type: 'table',
    title: 'inventory',
    items: {
        source: {
            type: 'var',
            uri: 'game/getInventory:personal'
        }
    },
    format: {
        id: {},
        qty: {}
    },
    subscribe: {
        'inventory-update': function(){
            this.reload();
        }
    }
};
ui_template['inventory/stats/table'] =  {
    id: 'stats_personal',
    type: 'table',
    title: 'stats',
    items: {
        source: {
            type: 'var',
            uri: 'player/getStats'
        }
    },
    format: {
        id: {},
        qty: function(item){
            return item.qty + '/' + item.max;
        }
    },
    subscribe: {
        'player-stats-update': function(){
            this.reload();
        },
        'flash': function(type){
            _dom(`.${this.id}-row.${type}`).style.cssText += 'animation-name: flash; animation-duration: 0.2s;';
        }
    }
};
ui_template['action/tabs/gang/table'] =  {
    id: 'action_tabs_gang',
    type: 'table',
    items: {
        source: {
            type: 'action_tab',
            uri: 'gang'
        }
    },
    format: {
        action: {
            type: 'button',
            prop: 'action'
        }
    },
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
};
ui_template['action/tabs/mine/table'] =  {
    id: 'action_tabs_mine',
    type: 'table',
    items: {
        source: {
            type: 'action_tab',
            uri: 'mine'
        }
    },
    format: {
        action: {
            type: 'button',
            source: {
                text: function(item){
                    return item.text;
                },
                action: function(item){
                    return item.action
                }
            }
        },
        info: {}
    },
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
};