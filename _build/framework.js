var components = {};

class ui_component{
    constructor(data){
        for (let prop in data){
            switch (prop) {
                case 'handler':
                    if (data.handler.source) {
                        this.handler = _data.source(data.handler.source);
                    }
                    else {
                        this.handler = data.handler;
                    }
                break;

                case 'items':
                    this.source = data.items.source ? data.items.source : data.items;
                break;

                case 'subscribe':
                    this.subscribtions = [];
                    for (let event in data[prop]){
                        this[event] = data[prop][event];
                        this.subscribtions.push(event);
                    }
                    this.subscribe = function(){
                        this.subscribtions.forEach((event) =>{
                            _event.subscribe(event, this);
                        })
                    }
                break;

                default:
                    this[prop] = data[prop];
                break;
            }
        }
    }

    load(component) {
        if (typeof component == 'string'){
            _dom(this).insertAdjacentHTML('beforeend', component);
        } else if (component instanceof ui_component){
            component.loadInto(this);
        }
    }
    loadInto(container) {
        _dom(container).insertAdjacentHTML('beforeend', this.section());
        this.initialize();
    }

    log() {
        console.log(this.id);
    }

    initialize(){
        if (this.id){
            components[this.id] = this;
        }
        if (this.source){
            this.prerender();
        } else {
            this.finishRender();
        }
    }

    prerender(){
        if (this.source.type == 'ajax'){
            fetch(this.source.uri, {})
            .then((response) => response.json())
            .then((data) => {
                this.items = data;
                this.finishRender();
            })
        } else {
            this.items = _data.source(this.source);
            this.finishRender();
        }
        
    }

    finishRender(){ 
        if (this.render){
            this.render();
        }
        if (this.listen){
            this.listen();
        }
        if (this.subscribe){
            this.subscribe();
        }
    }

    reload() {
        let old = _dom(this);
        while (old.firstChild) {
            old.removeChild(_dom(this).firstChild);
        }
        let replace = old.cloneNode(true);
        old.parentNode.replaceChild(replace, old);
        this.initialize();
    }
    section() {
        return `<div id="${this.id}" class="${this.baseClass + (this.class ? ` ${this.class}`  : '')}">`
    }

    setState(options = null) {
        if (options){
            for (let prop in options){
                this.state[prop] = options[prop];
            }
            this.reload();
        }
    }
}
(function(context){
    
    function Data_Sourcer(){

        this.source = function(source){
            if (source.type){
                if (!Object.keys(this).includes(source.type)){
                    return window[source.type][source.uri];
                } else {
                    return this[source.type](source.uri);
                }
            } else {
                return typeof source == 'function'? source() : source;
            }
            
        },
        this.literal = function(uri){
            return typeof uri == 'function' ? uri.apply() : uri;
        },
        this.var = function(uri, context = window){

            //uri = string
            //context = starting point of variable path generation

            //usage = takes a '/' separated path and searches the context object for the value
            //example = 'game/location/income:arg,arg,arg' => window.game.location.income(...args)
            let varParts = uri.split(":");
            let argArray = varParts[1] ? varParts[1].split(',') : [];
            let crumbs = varParts[0].split("/");
            if (context[crumbs[0]]){
                let bind = context[crumbs[0]];
                let path = context[crumbs.shift()];
                while (crumbs.length) {
                    if (path[crumbs[0]]){
                        path = path[crumbs.shift()];
                    }
                    else{
                        console.log('cannot locate ' + crumbs[0]);
                        return;
                    }
                }
                //if function, applies the function with 'this' = the next object after the context
                //example: window[game][theFunction] -- 'Game' is now 'this' in 'theFunction'
                return typeof path == 'function' ? path.apply(bind, argArray) : path;
            }
            console.log('cannot locate ' + crumbs[0]);
        }
    }

    if (!context._data){
        context._data = new Data_Sourcer();
    } else {
        console.log(context + '.' + '_data already exists');
    }

})(window);

function _dom(selector) {
    if (typeof selector == 'object'){
        return document.querySelector(`#${selector.id}`);
    } else {
        let selected = document.querySelectorAll(selector);
        return (selected.length > 1 ? selected : selected[0]);
    }
}
(function(context){

    function Event_Register(){
        this.list = {};
        
        this.subscribe = function(event, obj){
            if (!this.list[event]){
                this.list[event] = [];
            }
            if (this.list[event].indexOf(obj) == -1){
                this.list[event].push(obj);
            }
        }
    
        this.unsubscribe = function(obj, event = null){
            if (event){
                //remove the object from the event register
            } else {
                //remove the object from all event registers
            }
        }
    
        this.fire = function(event, ...args){
            if (this.list[event]){
                this.list[event].forEach((obj) =>{
                    obj[event].call(obj, ...args);
                });
            } else {
                console.log('no components are subscribed to event: ' + event);
            }
        }
    
        this.getList = function(){
            return list;
        }
    }

    if (!context._event){
        context._event = new Event_Register();
    } else {
        console.log(context + '.' + '_event already exists');
    }

})(window);


class button extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'button';
    }

    render(){
        if (this.action){
            _dom(this).dataset.action = this.action;
        }
        this.load(`${this.text ? this.text : this.id}`);
    }
}
class button_set extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'button_set';
    }

    render(){
        this.items.forEach((item) => {
            this.load(new button(item));
        })
    }

    listen(){
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('button')){
                this.handle(e);
            }
        });
    }

    handle(e){
        this.handler.handle(e)
    }
}
function dynamic(item){
    
        const component = {
            'screen': screen,
            'panel': panel,
            'button_set': button_set,
            'button': button,
            'list': list,
            'tab_set': tab_set,
            'table': table,
        }
        let template = item.source? _data.source(item.source) : item;
        return new component[template.type](template);
    }
class list extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'list';
    }

    render(){
        let html = this.items.reduce((str, item) => {
            return str + `<div class="notification">${item}</div>`
        }, '');
        this.load(html);
    }
}
class panel extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'panel';
    }

    render(){
        this.items.forEach((item) => {
            this.load(dynamic(item));
        });
    }
}
class screen extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'screen'
    }

    render(){
        this.items.forEach((item) => {
            dynamic(item).loadInto(this);
        });
    }
}
class table extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'table';
    }

    render(){
        if (this.items.length){
            this.load(`
            ${this.title ? `<div class="table-title"><strong class="table-title-text">${this.title}</strong></div>`: ''}
            <table>
                ${this.items.reduce((html, item) =>{
                    return html + this.row(item);
                }, '')}
            </table>
        `);
        }
    }

    row(item){
        var columns = Object.keys(this.format);

        return `
            <tr class="${this.id}-row ${item.id || null} row">${columns.reduce((html, column) => {
                    return html + this.cell(item, this.format[column], column);
            }, '')}</tr>
        `;
    }

    cell(item, column, key){
        if (column.type){
            switch(column.type){
                case 'button':
                    let data =  {id: null, action: null, text: null};
                    Object.keys(data).forEach((prop) => {
                        if (column.source){
                            if (column.source[prop]){
                                data[prop] = typeof column.source[prop] == 'function' ? column.source[prop](item) : column.source[prop];
                            }
                        } else if(item[prop]) {
                            data[prop] = item[prop];
                        } else {
                            data[prop] = '';
                        }
                    })
                    return `<td><div id="${data.id}" class="${key} button" data-action="${data.action}">
                        ${data.text}
                    </div></td>`;
                break;
            }
        }
        if (typeof column == 'function'){
            return `<td class="${key}">${column(item)}</td>`;
        }
        return `<td class="${key}">${typeof item[key] == 'function' ? item[key](item) : item[key]}</td>`; 
    }

    listen(){
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('button')){
                this.handle(e);
            }
        });
    }

    handle(e){
        this.handler.handle(e)
    }
}
class tab_set extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'tab_set';
    }

    render(){
        this.load(`
            <div class="tab_buttons">
                ${this.items.reduce((html, item) =>{
                    return html + this.tab_button(item, item.title == this.state.selected);
                }, '')}
            </div>
            <div class="tab_view"></div>
        `);
        dynamic(this.items.find((item) => {
            return item.title == this.state.selected;
        })).loadInto(`#${this.id} .tab_view`)
    }

    tab_button(item, selected){
        return `<div class="tab_button${selected ? ' selected' : ''}" data-title="${item.title}">${item.title}</div>`;
    }

    listen() {
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('tab_button')){
                this.handle(e);
            }
        });
    }

    handle(e) {
        this.setState({
            selected: e.target.dataset.title
        })
    }
}