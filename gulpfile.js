//required

var gulp    = require('gulp'),
    concat  = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    tap     = require('gulp-tap'),
    fs      = require('fs'),
    del     = require('del'),
    watch   = require('gulp-watch');


//tasks

gulp.task('app', ['global', 'game'], function(){
    
        gulp.src(['_build/game.js', '_build/global.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('_build'))
        .on('end', function(){
            del(['_build/global.js', '_build/game.js'])
            .then(function(){
            })
        })

        gulp.src('app/**/*.css')
        .pipe(concat('app.css'))
        .pipe(gulp.dest('_build'))
        
});

gulp.task('global', ['global-compile'], function(){
    
        return gulp.src(['_build/global_init.js', '_build/global_content.js'])
        .pipe(concat('global.js'))
        .pipe(gulp.dest('_build'))
        .on('end', function(){
            del(['_build/global_init.js', '_build/global_content.js'])
            .then(function(){
            })
        })
        
});

gulp.task('global-compile', function(){

    var initArray = [];

    return gulp.src('app/global/**/*.js')
    .pipe(tap(function(file){
        
        path = file.path;
        //switch slashes
        path = path.replace(/\\/gi, '/');

        //cut off everything up to the slash after 'global'
        path = path.slice(path.indexOf("global/") + 7);
        
        //create base variable name, and cut off the rest
        varName = path.slice(0);
        varName = varName.slice(0, varName.indexOf('/'));

        //cut from the first / to the .js for the property name
        prop = path.slice(path.indexOf('/') + 1, path.indexOf('.js'));

        if (!initArray.includes(varName)){
            initArray.push(varName);
        }
        name = `${varName}['${prop}'] = `;
        file.contents = Buffer.concat([
            new Buffer(name),
            //take off anything before the first 'equals' 
            file.contents.slice(file.contents.indexOf('=') + 1)
        ]);
    }))
    .pipe(concat('global_content.js'))
    .pipe(gulp.dest('_build'))
    .on('end', function(){
        fs.writeFileSync('_build/global_init.js', initArray.reduce((string, call) => {
            return string += (call + ' = {};\n' );
        }, ''))
    });

});


gulp.task ('framework', function() {
    gulp.src('framework/**/*.js')
    .pipe(concat('framework.js'))
    .pipe(gulp.dest('_build'))
    gulp.src('framework/**/*.css')
    .pipe(concat('framework.css'))
    .pipe(gulp.dest('_build'))
})

gulp.task ('game', function() {
    gulp.src('app/game/**/*.js')
    .pipe(concat('game.js'))
    .pipe(gulp.dest('_build'))
})

//watch task

gulp.task('watch', function() {
    watch(['app/**/*', 'framework/**/*'], function(){
        gulp.start('default');
    })
});

//default task

gulp.task('default', ['app','framework']);