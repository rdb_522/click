player = {
    stats: [],

    initialize(){
        this.addStat({health: 10, stamina: 10});
        this.changeStatMax({health: 10, stamina: 10});
    },

    getStats(){
        return this.stats.sort((a,b) => {
            return a.id > b.id;
        });
    },

    getStat(id){
        let found = this.stats.find((i) =>{
            return i.id == id
        });
        if (found){
            return found.qty
        } else {
            return false;
        }
    },

    changeStatMax(data){
        if (typeof data == 'array'){
            data.foreach((statList) => {
                this.changeStatMax(statList);
            })
        } else if(typeof data == 'object') {
            Object.keys(data).forEach((stat) => {
                let found = this.stats.find((i) =>{
                    return stat == i.id
                });
                if (found){
                    found.max = data[stat];
                } else {
                    this.stats.push({
                        id: stat,
                        qty: 0,
                        max: data[stat]
                    });
                }
                
            })
        }
        _event.fire('player-stats-update');
    },

    addStat(data) {
        if (typeof data == 'array'){
            data.foreach((statList) => {
                this.addStat(statList);
            })
        } else if(typeof data == 'object') {
            Object.keys(data).forEach((stat) => {
                let found = this.stats.find((i) =>{
                    return stat == i.id
                });
                if (found){
                    found.qty += data[stat];
                } else {
                    this.stats.push({
                        id: stat,
                        qty: data[stat]
                    });
                }
                
            })
        }
        _event.fire('player-stats-update');
    },

    tickStats(){
        this.stats.forEach((stat) => {
            stat.qty = stat.qty >= stat.max ? stat.qty : stat.qty + 1;
        })
        _event.fire('player-stats-update');
    }
}