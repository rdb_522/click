game = {
    inventory: {
        personal: []
    },

    state: {
        stage: 1
    },

    getInventory(location){
        if (this.inventory[location]){
            return this.inventory[location].sort((a,b) => {
                return a.id > b.id;
            });
        } else {
            return [];
        }
    },

    addInventory(location, id, qty){
        let found = this.inventory[location].find((i) =>{
            return i.id == id
        });
        if (found){
            found.qty += qty;
        } else {
            this.inventory[location].push({
                id: id,
                qty: qty,
                max: qty
            });
        }
        _event.fire('inventory-update');
    },

    initialize(){
        _event.subscribe('tick', this);
        player.initialize();
        clock.initialize();
    },

    tick(){
        player.tickStats();
    }
}