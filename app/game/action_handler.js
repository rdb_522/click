action_handler = {

    handle(e) {
        if (e.target){
            if (e.target.dataset){
                if (e.target.dataset.action){
                    let actionData = e.target.dataset.action;
                    if (action[actionData]){
                        actionData = action[actionData];
                        if (actionData.requires){
                            //if requires property is a function
                            if (typeof actionData.requires == 'function'){
                                if(!actionData.requires(e)){
                                    log.add('requirements not met for: ' + actionData.text);
                                    return false;
                                }
                            //if the requires property is the standard 'check/requires' object
                            } else if (Array.isArray(actionData.requires)) {
                                let errors = this.processRequirements(actionData.requires);
                                if (errors.length > 0) {
                                    errors.forEach((error)=>{
                                        error.call();
                                    })
                                    return false;
                                }
                            }
                        }
                        actionData.action(e);
                        return true;
                    } else {
                        console.log('cannot locate action: ' + actionData)
                        return false;
                    }
                }
            }
        }
        console.log(e);
    },
    processRequirements(needs){
        let errors = [];
        needs.forEach((item) => {
            if (!item.check.call()){
                errors.push(item.fail);
            }
        });
        return errors;
    }
}