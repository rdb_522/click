clock = {
    state: {
        started: false,
        running: false,
    },
    initialize(){
        _event.subscribe('pause',this);
        this.run();
        this.state.started = true;
    },

    stop(){
        window.clearInterval(gameTime);
        this.state.running = false;
    },

    run(){
        this.state.running = true;
        window.gameTime = window.setInterval(function(){
            _event.fire('tick');
        }, 1000);
    }

}