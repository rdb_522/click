template = {
    id: 'stats_personal',
    type: 'table',
    title: 'stats',
    items: {
        source: {
            type: 'var',
            uri: 'player/getStats'
        }
    },
    format: {
        id: {},
        qty: function(item){
            return item.qty + '/' + item.max;
        }
    },
    subscribe: {
        'player-stats-update': function(){
            this.reload();
        },
        'flash': function(type){
            _dom(`.${this.id}-row.${type}`).style.cssText += 'animation-name: flash; animation-duration: 0.2s;';
        }
    }
};