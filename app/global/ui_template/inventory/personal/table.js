template = {
    id: 'inventory_personal',
    type: 'table',
    title: 'inventory',
    items: {
        source: {
            type: 'var',
            uri: 'game/getInventory:personal'
        }
    },
    format: {
        id: {},
        qty: {}
    },
    subscribe: {
        'inventory-update': function(){
            this.reload();
        }
    }
};