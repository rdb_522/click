template = {
    id: 'inventory',
    type: 'panel',
    items: [
        {
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'inventory/stats/table'
            }
        },
        {
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'inventory/personal/table'
            }
        }
    ]

};