template = {
    id: 'menu',
    type: 'button_set',
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
    items: [
        {
            id: 'pause',
            action: 'system/pause'
        },
        {
            id: 'about',
            action: 'system/info',
            text: 'info'
        }
    ]
}