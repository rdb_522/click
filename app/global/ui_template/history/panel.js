template = {
    id: 'history',
    type: 'panel',
    items: [
        {
            source: {
                type: 'ui_template',
                uri: 'history/menu/buttons'
            }
        },
        {
            source: {
                type: 'ui_template',
                uri: 'history/log/list'
            }
        }
    ]
};