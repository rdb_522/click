template = {
    id: 'log',
    type: 'list',
    items: {
        source: {
            type: 'var',
            uri: 'log/getItems'
        }
    },
    subscribe: {
        'logUpdate': function(){
            this.reload();
        }
    }
};