template = {
    id: 'action_tabs',
    type: 'tab_set',
    items: [
        {
            title: 'mine',
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'action/tabs/mine/table'
            }
        },
        {
            title: 'gang',
            type: 'table',
            source: {
                type: 'ui_template',
                uri: 'action/tabs/gang/table'
            }
        }
    ],
    state: {
        selected: 'mine'
    }
    
};