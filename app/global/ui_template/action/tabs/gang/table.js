template = {
    id: 'action_tabs_gang',
    type: 'table',
    items: {
        source: {
            type: 'action_tab',
            uri: 'gang'
        }
    },
    format: {
        action: {
            type: 'button',
            prop: 'action'
        }
    },
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
};