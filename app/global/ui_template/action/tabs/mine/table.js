template = {
    id: 'action_tabs_mine',
    type: 'table',
    items: {
        source: {
            type: 'action_tab',
            uri: 'mine'
        }
    },
    format: {
        action: {
            type: 'button',
            source: {
                text: function(item){
                    return item.text;
                },
                action: function(item){
                    return item.action
                }
            }
        },
        info: {}
    },
    handler: {
        source: {
            type: 'var',
            uri: 'action_handler'
        }
    },
};