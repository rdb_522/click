mine = [
    {
        id: 'sand',
        text: 'grab sand',
        action: 'player/mine/sand',
        info: '1 stamina'
    },
    {
        id: 'rock',
        text: 'grab rock',
        action: 'player/mine/rock',
        info: '2 stamina'
    },
    {
        id: 'metal',
        text: 'mine metal',
        action: 'player/mine/metal',
        info: '5 stamina'
    },
]