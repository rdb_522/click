pause = {
    action: function (e){
        if (clock.state.running){
            clock.stop();
        } else {
            clock.run();
        }
        console.log(clock.state.running ? 'Clock Running' : 'Clock Stopped');
    }
}