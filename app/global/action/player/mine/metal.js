metal = {
    text: 'mine metal',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 5;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -5});
        game.addInventory('personal', 'metal', 1);
    }
}