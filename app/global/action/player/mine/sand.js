sand = {
    text: 'grab sand',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 1;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -1});
        game.addInventory('personal', 'sand', 1);
    }
}