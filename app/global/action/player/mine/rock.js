rock = {
    text: 'grab rock',
    requires: [
        {
            check: function(e){
                return player.getStat('stamina') >= 2;
            },
            fail: function(e){
                _event.fire('flash', 'stamina');
            }
        }
    ],
    action: function (e){
        player.addStat({stamina: -2});
        game.addInventory('personal', 'rock', 1);
    }
}