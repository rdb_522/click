function _dom(selector) {
    if (typeof selector == 'object'){
        return document.querySelector(`#${selector.id}`);
    } else {
        let selected = document.querySelectorAll(selector);
        return (selected.length > 1 ? selected : selected[0]);
    }
}