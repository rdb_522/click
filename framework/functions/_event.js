(function(context){

    function Event_Register(){
        this.list = {};
        
        this.subscribe = function(event, obj){
            if (!this.list[event]){
                this.list[event] = [];
            }
            if (this.list[event].indexOf(obj) == -1){
                this.list[event].push(obj);
            }
        }
    
        this.unsubscribe = function(obj, event = null){
            if (event){
                //remove the object from the event register
            } else {
                //remove the object from all event registers
            }
        }
    
        this.fire = function(event, ...args){
            if (this.list[event]){
                this.list[event].forEach((obj) =>{
                    obj[event].call(obj, ...args);
                });
            } else {
                console.log('no components are subscribed to event: ' + event);
            }
        }
    
        this.getList = function(){
            return list;
        }
    }

    if (!context._event){
        context._event = new Event_Register();
    } else {
        console.log(context + '.' + '_event already exists');
    }

})(window);

