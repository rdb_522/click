(function(context){
    
    function Data_Sourcer(){

        this.source = function(source){
            if (source.type){
                if (!Object.keys(this).includes(source.type)){
                    return window[source.type][source.uri];
                } else {
                    return this[source.type](source.uri);
                }
            } else {
                return typeof source == 'function'? source() : source;
            }
            
        },
        this.literal = function(uri){
            return typeof uri == 'function' ? uri.apply() : uri;
        },
        this.var = function(uri, context = window){

            //uri = string
            //context = starting point of variable path generation

            //usage = takes a '/' separated path and searches the context object for the value
            //example = 'game/location/income:arg,arg,arg' => window.game.location.income(...args)
            let varParts = uri.split(":");
            let argArray = varParts[1] ? varParts[1].split(',') : [];
            let crumbs = varParts[0].split("/");
            if (context[crumbs[0]]){
                let bind = context[crumbs[0]];
                let path = context[crumbs.shift()];
                while (crumbs.length) {
                    if (path[crumbs[0]]){
                        path = path[crumbs.shift()];
                    }
                    else{
                        console.log('cannot locate ' + crumbs[0]);
                        return;
                    }
                }
                //if function, applies the function with 'this' = the next object after the context
                //example: window[game][theFunction] -- 'Game' is now 'this' in 'theFunction'
                return typeof path == 'function' ? path.apply(bind, argArray) : path;
            }
            console.log('cannot locate ' + crumbs[0]);
        }
    }

    if (!context._data){
        context._data = new Data_Sourcer();
    } else {
        console.log(context + '.' + '_data already exists');
    }

})(window);
