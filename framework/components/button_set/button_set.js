class button_set extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'button_set';
    }

    render(){
        this.items.forEach((item) => {
            this.load(new button(item));
        })
    }

    listen(){
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('button')){
                this.handle(e);
            }
        });
    }

    handle(e){
        this.handler.handle(e)
    }
}