class list extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'list';
    }

    render(){
        let html = this.items.reduce((str, item) => {
            return str + `<div class="notification">${item}</div>`
        }, '');
        this.load(html);
    }
}