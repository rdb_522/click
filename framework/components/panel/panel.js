class panel extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'panel';
    }

    render(){
        this.items.forEach((item) => {
            this.load(dynamic(item));
        });
    }
}