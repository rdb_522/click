class screen extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'screen'
    }

    render(){
        this.items.forEach((item) => {
            dynamic(item).loadInto(this);
        });
    }
}