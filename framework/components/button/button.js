class button extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'button';
    }

    render(){
        if (this.action){
            _dom(this).dataset.action = this.action;
        }
        this.load(`${this.text ? this.text : this.id}`);
    }
}