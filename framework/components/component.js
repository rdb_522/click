var components = {};

class ui_component{
    constructor(data){
        for (let prop in data){
            switch (prop) {
                case 'handler':
                    if (data.handler.source) {
                        this.handler = _data.source(data.handler.source);
                    }
                    else {
                        this.handler = data.handler;
                    }
                break;

                case 'items':
                    this.source = data.items.source ? data.items.source : data.items;
                break;

                case 'subscribe':
                    this.subscribtions = [];
                    for (let event in data[prop]){
                        this[event] = data[prop][event];
                        this.subscribtions.push(event);
                    }
                    this.subscribe = function(){
                        this.subscribtions.forEach((event) =>{
                            _event.subscribe(event, this);
                        })
                    }
                break;

                default:
                    this[prop] = data[prop];
                break;
            }
        }
    }

    load(component) {
        if (typeof component == 'string'){
            _dom(this).insertAdjacentHTML('beforeend', component);
        } else if (component instanceof ui_component){
            component.loadInto(this);
        }
    }
    loadInto(container) {
        _dom(container).insertAdjacentHTML('beforeend', this.section());
        this.initialize();
    }

    log() {
        console.log(this.id);
    }

    initialize(){
        if (this.id){
            components[this.id] = this;
        }
        if (this.source){
            this.prerender();
        } else {
            this.finishRender();
        }
    }

    prerender(){
        if (this.source.type == 'ajax'){
            fetch(this.source.uri, {})
            .then((response) => response.json())
            .then((data) => {
                this.items = data;
                this.finishRender();
            })
        } else {
            this.items = _data.source(this.source);
            this.finishRender();
        }
        
    }

    finishRender(){ 
        if (this.render){
            this.render();
        }
        if (this.listen){
            this.listen();
        }
        if (this.subscribe){
            this.subscribe();
        }
    }

    reload() {
        let old = _dom(this);
        while (old.firstChild) {
            old.removeChild(_dom(this).firstChild);
        }
        let replace = old.cloneNode(true);
        old.parentNode.replaceChild(replace, old);
        this.initialize();
    }
    section() {
        return `<div id="${this.id}" class="${this.baseClass + (this.class ? ` ${this.class}`  : '')}">`
    }

    setState(options = null) {
        if (options){
            for (let prop in options){
                this.state[prop] = options[prop];
            }
            this.reload();
        }
    }
}