class tab_set extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'tab_set';
    }

    render(){
        this.load(`
            <div class="tab_buttons">
                ${this.items.reduce((html, item) =>{
                    return html + this.tab_button(item, item.title == this.state.selected);
                }, '')}
            </div>
            <div class="tab_view"></div>
        `);
        dynamic(this.items.find((item) => {
            return item.title == this.state.selected;
        })).loadInto(`#${this.id} .tab_view`)
    }

    tab_button(item, selected){
        return `<div class="tab_button${selected ? ' selected' : ''}" data-title="${item.title}">${item.title}</div>`;
    }

    listen() {
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('tab_button')){
                this.handle(e);
            }
        });
    }

    handle(e) {
        this.setState({
            selected: e.target.dataset.title
        })
    }
}