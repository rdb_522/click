function dynamic(item){
    
        const component = {
            'screen': screen,
            'panel': panel,
            'button_set': button_set,
            'button': button,
            'list': list,
            'tab_set': tab_set,
            'table': table,
        }
        let template = item.source? _data.source(item.source) : item;
        return new component[template.type](template);
    }