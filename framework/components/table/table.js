class table extends ui_component{
    constructor(data){
        super(data);
        this.baseClass = 'table';
    }

    render(){
        if (this.items.length){
            this.load(`
            ${this.title ? `<div class="table-title"><strong class="table-title-text">${this.title}</strong></div>`: ''}
            <table>
                ${this.items.reduce((html, item) =>{
                    return html + this.row(item);
                }, '')}
            </table>
        `);
        }
    }

    row(item){
        var columns = Object.keys(this.format);

        return `
            <tr class="${this.id}-row ${item.id || null} row">${columns.reduce((html, column) => {
                    return html + this.cell(item, this.format[column], column);
            }, '')}</tr>
        `;
    }

    cell(item, column, key){
        if (column.type){
            switch(column.type){
                case 'button':
                    let data =  {id: null, action: null, text: null};
                    Object.keys(data).forEach((prop) => {
                        if (column.source){
                            if (column.source[prop]){
                                data[prop] = typeof column.source[prop] == 'function' ? column.source[prop](item) : column.source[prop];
                            }
                        } else if(item[prop]) {
                            data[prop] = item[prop];
                        } else {
                            data[prop] = '';
                        }
                    })
                    return `<td><div id="${data.id}" class="${key} button" data-action="${data.action}">
                        ${data.text}
                    </div></td>`;
                break;
            }
        }
        if (typeof column == 'function'){
            return `<td class="${key}">${column(item)}</td>`;
        }
        return `<td class="${key}">${typeof item[key] == 'function' ? item[key](item) : item[key]}</td>`; 
    }

    listen(){
        _dom(this).addEventListener('click', (e) => {
            if (e.target.classList.contains('button')){
                this.handle(e);
            }
        });
    }

    handle(e){
        this.handler.handle(e)
    }
}